package co.com.cubesummation;

import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CubeSummationApplication {

	static final Scanner SCANNER = new Scanner(System.in);
	static int cubo[][][];
	static int row;
	static int column;
	static int layer;

	public static void main(String[] args) {
		SpringApplication.run(CubeSummationApplication.class, args);

		buildCube(0);
		displayCube();

		System.out.println("\t\t<<< Cube Summation Started >>>");
		System.out.println("Enter the number of test-cases");

		int testCase = SCANNER.nextInt();
		int count = 0;
		int n = 0;
		int m = 0;
		String n_m = null;

		do {
			testCase--;
			System.out.print("_> ");
			System.out.print("Enter two integers N and M separated by a single space ");
			n_m = SCANNER.next();
			n = Integer.parseInt(n_m.split(" ")[0]);
			m = Integer.parseInt(n_m.split(" ")[1]);

			buildCube(n);

		} while (testCase == 0);
	}

	static void buildCube(int n) {
		row = n;
		column = n;
		layer = n;
		cubo = new int[row][column][layer];
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				for (int k = 0; k < layer; k++) {
					cubo[i][j][k] = n;
				}
			}
		}
	}

	static void displayCube() {
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				for (int k = 0; k < layer; k++) {
					// System.out.print("\t" + cubo[i][j][k]);
					System.out.print("cubo[" + i + "][" + j + "][" + k + "] = " + cubo[i][j][k] + "\t");
				}
				System.out.println();
			}
			System.out.println();
		}
	}

}

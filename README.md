cube-summation es una proyecto Maven tipo web, el cual lo componen dos capas. Una capa de presentación y una capa de lógica.

**CubeSummationController** Es la clase que hace parte de la capa de presentación. Aquí se expondrá un servicio rest que será consumido para ejecutar la aplicación

**CubeSummationService** Es una interfaz que será usada para abstraer la lógica de negocio. 

**CubeSummationServiceImpl** Clase que implementa la interfaz mencionada ateriormente, aquí será desarrollada la lógica de la aplicación


**NOTA:** Para efectos de pruebas se esta haciendo uso de la clase principal del proyecto (**CubeSummationApplication**) en la que por consola se están capturando los datos para trabajar con el Cubo.



